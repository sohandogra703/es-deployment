"""Module providing main function."""
#!/usr/bin/env python3

import os
import argparse
import yaml
import os
import shutil
import shutil
import itertools
"""Module providing main function."""
#!/usr/bin/env python3
CONF_PATH_ENV_KEY = "CONF_PATH"

class DeplyomentES:
    """Class for generation template of ES as per the namespace """
    def scan_resources(self, arg):
        """Function for reading the configuration """        
        with open(ARGS.property_file_path, "r", encoding="utf-8") as load_file:
            properties = yaml.load(load_file, Loader=yaml.FullLoader)
            template_file =ARGS.template_file
            destination_path=ARGS.manifest_file_path
            namespace = ARGS.namespace
            environment = properties['enviornment']
            if template_file and destination_path :                
                if  namespace in  environment:                 
                        NAMESPACE=namespace       
                        REQUEST_CPU=environment[namespace]['cpu_limit']
                        LIMIT_MEMORY=environment[namespace]['mem_limt']
                        REQUEST_MEMORY=environment[namespace]['mem_request']
                        SECRET_NAME=environment[namespace]['secretName']
                        STORAGE_CLASS=environment[namespace]['stroageClass'] 
                        environment_dir=os.path.join(destination_path,namespace)  
                        destination_path_file = os.path.join(
                            environment_dir+"/elasticsearch-"+namespace+".yaml")  
                        environment_dir_exist=os.path.exists(environment_dir)
                        if not environment_dir_exist :
                            os.mkdir(environment_dir) 
                            print(f"{namespace} dir not exist,so creating it ") 
                        else :
                            os.remove(destination_path_file)    
                        shutil.copy2(template_file, destination_path_file)
                        replace_data = {
                                        "NAMESPACE": NAMESPACE,
                                        "REQUEST_MEMORY": REQUEST_MEMORY,
                                        "LIMIT_MEMORY": LIMIT_MEMORY,
                                        "REQUEST_CPU": REQUEST_CPU ,   
                                        "SECRET_NAME" : SECRET_NAME ,
                                        "STORAGE_CLASS" : STORAGE_CLASS
                                    }
                        self.replace_engine(environment_dir, replace_data) 
                        print(f"mainfest of given env:{namespace} generated successfully")
                else:
                        print (f"namespace:{namespace}  doesnot exist in evnvironment of configuration.yaml ") 
            else :
                    print(f"{properties['template_file']} and {properties['manifest_file_path']} cannot be empty ")                
    def replace_engine(self, environment_dir, replace_data):
        """ Method for  replace  value in template  """
        listkeys = []
        listvalues = []
        for key in replace_data.keys():
            listkeys.append(key)         
        for value in replace_data.values():
            listvalues.append(value) 
        with os.scandir(environment_dir) as listi_item:
            for entry in listi_item:
                if not entry.is_file():
                    continue
                with open(entry.path, 'r',encoding="utf-8") as file:
                    filedata = file.read()
                for (replace_word, replace_value) in itertools.zip_longest(listkeys, listvalues):
                    filedata = filedata.replace(
                        str(replace_word), str(replace_value))
                    with open(entry.path, 'w',encoding="utf-8") as file:
                        file.write(filedata)
if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("-p", "--property-file-path", help="Provide path of property file",
                        default=os.environ[CONF_PATH_ENV_KEY], type=str)
    PARSER.add_argument('-n',"--namespace",type=str)
    PARSER.add_argument('-t', "--template_file", type=str)
    PARSER.add_argument('-m',"--manifest_file_path",type=str)
    ARGS = PARSER.parse_args()
    DeplyomentES = DeplyomentES()
    DeplyomentES.scan_resources(ARGS)
